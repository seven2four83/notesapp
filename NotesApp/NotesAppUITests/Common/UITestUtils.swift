//
//  UITestUtils.swift
//  NotesAppUITests
//
//  Created by Jagan Moorthy on 29/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import XCTest

var lastCreatedNote: Note? = nil

struct UITestUtils{
    
    static func getLatestNoteOrCreateDresh(app: XCUIApplication) -> Note{
        guard let lastCreatedNote = lastCreatedNote else {
            return createNote(app: app)
        }
        return lastCreatedNote
    }
    
    static func createNote(app: XCUIApplication) -> Note{
        let note = getNoteWithCurrentTimeStamp()
        app.buttons["Add"].tap()
        let titleTextField = app.textFields["Title"]
        titleTextField.tap()
        titleTextField.typeText(note.title)
        let contentTextView = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textView).element
        contentTextView.tap()
        contentTextView.typeText(note.noteContent.text)
        saveNote(note, app:app )
        return note
    }
    
    static func saveNote(_ note:Note, app: XCUIApplication){
        app.navigationBars["Master"].buttons["Save"].tap()
        lastCreatedNote = note
    }
    
    private static func getNoteWithCurrentTimeStamp() -> Note{
        let timeStamp = Date()
        let timeStampString = String(Int(timeStamp.timeIntervalSince1970))
        var note = BaseNote()
        note.title = "XCUITestTitle\(timeStampString)"
        note.noteContent.text = "XCUITestContent\(timeStampString)"
        return note
    }
    
    static func openNoteInEditMode(app: XCUIApplication, note: Note){
        app.tables[Constants.AccessibilityIdentifiers.ListingVC.allNotesTable].staticTexts[note.title].tap()
        let masterNavigationBar = app.navigationBars["Master"]
        masterNavigationBar.buttons["Edit"].tap()
    }
}
