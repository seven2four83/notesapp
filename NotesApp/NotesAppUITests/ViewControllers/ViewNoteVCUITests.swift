//
//  ViewNoteVCUITests.swift
//  NotesAppUITests
//
//  Created by Jagan Moorthy on 01/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class ViewNoteVCUITests: XCTestCase {
    
    var app:XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testAllViewsPresent(){
        
        let note = UITestUtils.createNote(app: app)
        
        app.tables.children(matching: .cell).element(boundBy: 0).tap()
        
        let masterNavigationBar = app.navigationBars["Master"]
        XCTAssertTrue(masterNavigationBar.buttons["Edit"].exists)
        XCTAssertTrue(app.staticTexts[note.title].exists)
//        XCTAssertTrue(app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textView).element.exists)
        let textView = app.textViews[Constants.AccessibilityIdentifiers.ViewNoteVC.viewContentTextView]
        XCTAssertEqual(textView.value as? String, note.noteContent.text)
    }

    
    func testEditTap(){
        let note = UITestUtils.createNote(app: app)
        
        UITestUtils.openNoteInEditMode(app: app, note: note)
        let textView = app.textViews[Constants.AccessibilityIdentifiers.EditNoteVC.contentTextView]
        XCTAssertEqual(textView.value as? String, note.noteContent.text)
        
        let titleView = app.textFields[Constants.AccessibilityIdentifiers.EditNoteVC.titleTextField]
        XCTAssertEqual(titleView.value as? String, note.title)
        let masterNavigationBar = app.navigationBars["Master"]
        masterNavigationBar.buttons["Back"].tap()
        
    }
}
