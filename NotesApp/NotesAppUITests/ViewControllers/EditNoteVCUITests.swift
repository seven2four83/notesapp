//
//  EditNoteVCUITests.swift
//  NotesAppUITests
//
//  Created by Jagan Moorthy on 31/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

class EditNoteVCUITests: XCTestCase {
    var app:XCUIApplication!
    var undeditedNote: Note!
    var editedNote: Note!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        undeditedNote = UITestUtils.getLatestNoteOrCreateDresh(app: app)
        configureEditedNote()
        UITestUtils.openNoteInEditMode(app: app, note: undeditedNote)
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    private func configureEditedNote(){
        editedNote = BaseNote()
        let timeStamp = Date()
        let timeStampString = String(Int(timeStamp.timeIntervalSince1970))
        editedNote.title = undeditedNote.title + timeStampString
        editedNote.noteContent.text = undeditedNote.noteContent.text + timeStampString
    }

//    func testExample() {
//        // Use recording to get started writing UI tests.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
    
    func fillDetails(withNote note: Note, app: XCUIApplication){
        let titleTextField = app.textFields[Constants.AccessibilityIdentifiers.EditNoteVC.titleTextField]
        titleTextField.press(forDuration: 1.2)
        app.menuItems["Select All"].tap()
        titleTextField.typeText(note.title)
        let contentTextView = app.textViews[Constants.AccessibilityIdentifiers.EditNoteVC.contentTextView]
        contentTextView.tap()
        contentTextView.press(forDuration: 1.2)
        app.menuItems["Select All"].tap()
        contentTextView.typeText(note.noteContent.text)
        //TODO: Haven't really asserted anything yet!
    }
    
    func testEditAndSave(){
        fillDetails(withNote: editedNote, app: app)
        UITestUtils.saveNote(editedNote, app: app)
        sleep(1)
        XCTAssertTrue(app.staticTexts[editedNote.title].exists)
        let textView = app.textViews[Constants.AccessibilityIdentifiers.ViewNoteVC.viewContentTextView]
        XCTAssertEqual(textView.value as? String, editedNote.noteContent.text)
    }

}
