//
//  NotesAppUITests.swift
//  NotesAppUITests
//
//  Created by Jagan Moorthy on 27/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest

final class NotesAppUITests: XCTestCase {
    
    var app:XCUIApplication!
    
    var firstCell: XCUIElement{
        return app.tables[Constants.AccessibilityIdentifiers.ListingVC.allNotesTable].cells.element(boundBy: 0)
    }
    
    var firstSearchCell: XCUIElement{
        return app.tables[Constants.AccessibilityIdentifiers.ListingVC.searchTable].cells.element(boundBy: 0)
    }
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchTap(){
        XCTAssertTrue(app.buttons["Add"].exists)
        XCTAssertTrue(app.buttons["Search"].exists)
        XCTAssertTrue(app.buttons[Constants.AccessibilityIdentifiers.ListingVC.ListingTitleBar.leftButton].exists)
    }
    
    
    func testCreateNote(){
        let note = UITestUtils.createNote(app: app)
        XCTAssertTrue(firstCell.staticTexts[note.title].exists)
    }
    
    func testDeleteNote(){
        let note = UITestUtils.createNote(app: app)
        XCTAssertTrue(app.tables[Constants.AccessibilityIdentifiers.ListingVC.allNotesTable].staticTexts[note.title].exists)
//        let firstCell = app.tables[Constants.AccessibilityIdentifiers.ListingVC.allNotesTable].cells.element(boundBy: 0)
        firstCell.swipeLeft()
        firstCell.buttons["Delete"].tap()
        XCTAssertFalse(app.tables[Constants.AccessibilityIdentifiers.ListingVC.allNotesTable].staticTexts[note.title].exists)
    }
    
    func testTextOnlyCellSubviews(){
        let note = UITestUtils.createNote(app: app)
        if !firstCell.staticTexts["Today"].exists{
            app/*@START_MENU_TOKEN@*/.buttons["leftButton"]/*[[".buttons[\"list\"]",".buttons[\"leftButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        }
        XCTAssertTrue(firstCell.staticTexts["Today"].exists)
        XCTAssertTrue(firstCell.staticTexts[note.title].exists)
        XCTAssertTrue(firstCell.staticTexts[note.noteContent.text].exists)
    }
    
    func testSearch(){
        let note = UITestUtils.createNote(app: app)
        app.buttons[Constants.AccessibilityIdentifiers.ListingVC.ListingTitleBar.searchButton].tap()
        let searchTextField = app.textFields[Constants.AccessibilityIdentifiers.ListingVC.ListingTitleBar.searchTextField]
        searchTextField.tap()
        searchTextField.typeText(note.title)
        XCTAssertTrue(firstSearchCell.staticTexts[note.title].exists)
    }
    
}
