//
//  ListingViewControllerTests.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 20/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest
@testable import NotesApp

final class ListingViewControllerTests: XCTestCase {
    
    var sut:ListingViewController!

    override func setUp() {
        super.setUp()
        let splitVC = UIStoryboard(name: "Main", bundle: Bundle(for: self.classForCoder)).instantiateInitialViewController() as! UISplitViewController
        let navVC = splitVC.viewControllers.first as! UINavigationController
        sut = (navVC.topViewController as! ListingViewController)
        
        XCTAssertNil(sut.titleBar?.delegate)
        XCTAssertNil(sut.allNotesTable?.notesTableHandler)
        
        let _ = sut.view
        
        
    }
    
    func testViewDidLoad(){
        XCTAssertEqual((sut.titleBar.delegate as! ListingViewController), sut)
        let allNotesTableHandler = sut.allNotesTable.notesTableHandler!
        XCTAssertEqual((allNotesTableHandler.model as! ListingViewController), sut)
        XCTAssertEqual(allNotesTableHandler.parentVC, sut)
        XCTAssertNotNil(allNotesTableHandler.persistenceManager)
        XCTAssertFalse(allNotesTableHandler.isSearchMode)
    }
    
    func testDidTapAddButton(){
        sut.didTapAddButton()
       let editNoteVC =  sut.navigationController?.topViewController as! EditNoteViewController
        XCTAssertEqual((editNoteVC.delegate as! ListingViewController), sut)
    }
    
    func testSearch(){
        
//        XCTAssertNil(sut.searchNotesTable.notesTableHandler)
//        XCTAssertTrue(sut.searchNotesTable.isHidden)
//
//
//        sut.search(quertyString: "1")
//
//        sut.search(quertyString: "")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

//    func testExample() {
//        // Use recording to get started writing UI tests.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }

}
