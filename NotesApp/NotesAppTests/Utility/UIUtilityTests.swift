//
//  UIUtilityTests.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 20/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest
import UIKit
@testable import NotesApp

final class UIUtilityTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSingleActionAlert(){
        let viewController = UIViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
        UIUtility.showSingleActionAlert(title: "testTitle", message: "testMessage", viewController: viewController)
        XCTAssertTrue(viewController.presentedViewController is UIAlertController)
        
        let alertVC = viewController.presentedViewController as! UIAlertController
        
        XCTAssertEqual(alertVC.title, "testTitle")
        XCTAssertEqual(alertVC.message, "testMessage")
        XCTAssertEqual(alertVC.actions.count, 1)
        
        let okayAction = alertVC.actions[0]
        XCTAssertEqual(okayAction.title, "OK")
        XCTAssertEqual(okayAction.style, .cancel)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
