//
//  SearchHandlerTests.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 15/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import XCTest

@testable import NotesApp

final class SearchHandlerTests: XCTestCase{
    
    func testCaseInSensitiveNotesFilter() {
        var notes = [Note]()
        
        notes.append(BaseNote(title: "testNoteA", dateCreated: Date(), dateModified: nil, persistenceManager: nil, noteId: -1, noteContent: NoteContent(), isFreshNote: false))
        notes.append(BaseNote(title: "testNotea", dateCreated: Date(), dateModified: nil, persistenceManager: nil, noteId: -1, noteContent: NoteContent(), isFreshNote: false))
        notes.append(BaseNote(title: "testNoteC", dateCreated: Date(), dateModified: nil, persistenceManager: nil, noteId: -1, noteContent: NoteContent(), isFreshNote: false))
        
        XCTAssertEqual(SearchHandler.filterNotes(allNotes: notes, withSearchQuery: "a").count, 2)
        XCTAssertEqual(SearchHandler.filterNotes(allNotes: notes, withSearchQuery: "C").count, 1)
        XCTAssertEqual(SearchHandler.filterNotes(allNotes: notes, withSearchQuery: "xyz").count, 0)
        
    }
    
}
