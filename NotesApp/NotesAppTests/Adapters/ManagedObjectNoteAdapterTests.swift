//
//  ManagedObjectNoteAdapterTests.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 03/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import XCTest
import CoreData
@testable import NotesApp

final class ManagedObjectNoteAdapterTests: XCTestCase {
    
    var note:Note!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        note = BaseNote()
        note.noteId = 1
        note.title = "XCTest note"
        let dateOfReference = Date()
        note.dateCreated = Date(timeInterval: -(60*60*24), since: dateOfReference)
        note.dateModified = dateOfReference
    }
    
    
    func testValidNoteConfigurationForExistingNote() {
//        let noteObject = NSManagedObject()
//        ManagedObjectNoteAdapter.configure(noteObject: noteObject, withNote: note, isFreshNore: false)
//        XCTAssertEqual(noteObject.value(forKey: ManagedObjectNoteAdapter.ObjectKeyStrings.titleKey) as! String, note.title)
//        XCTAssertEqual(noteObject.value(forKey: ManagedObjectNoteAdapter.ObjectKeyStrings.dateCreatedKey) as! Date , note.dateCreated)
//        XCTAssertEqual(noteObject.value(forKey: ManagedObjectNoteAdapter.ObjectKeyStrings.dateModifiedKey) as! Date , note.dateModified)
//        XCTAssertEqual(noteObject.value(forKey: ManagedObjectNoteAdapter.ObjectKeyStrings.noteIdKey) as! NSNumber , note.noteId)
//        XCTAssertEqual(noteObject.value(forKey: ManagedObjectNoteAdapter.ObjectKeyStrings.contentKey) as! String , note.)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        note = nil
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
