//
//  MockPersistentManager.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 15/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

@testable import NotesApp

final class MockPersistenceManager: PersistenceManager{
    func updateNote(note: Note) -> Bool {
        return false
    }
    
    var notes = [Note]()
    func getAllNotes(sortOrder: Constants.SortOrder, sortDirection: Constants.SortDirection) -> [Note] {
        return notes
    }
    
    func saveNote(note: Note) -> Bool {
        notes.append(note)
        return true
    }
    
    func deleteNote(note: Note) -> Bool {
        notes = [Note]()
        return false
    }
    
}
