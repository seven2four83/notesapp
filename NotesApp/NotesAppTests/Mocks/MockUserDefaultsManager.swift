//
//  MockUserDefaultsManager.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 17/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

@testable import NotesApp

final class MockUserDefaultsKeys : UserDefaultsKeys{
    override init(){
        super.init()
        runningNoteIdUserDefaultsKey = "MockrunningNoteIdUserDefaultsKey"
        cellStyleUserDefaultsKey = "MockcellStyleUserDefaultsKey"
    }
}

final class MockUserDefaultsManager: UserDefaultsManager{
//    init() {
//        super.init(userDefaultKeys: MockUserDefaultsKeys())
//    }
}
