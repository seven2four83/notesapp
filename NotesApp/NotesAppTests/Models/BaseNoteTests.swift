//
//  BaseNoteTests.swift
//  NotesAppTests
//
//  Created by Jagan Moorthy on 17/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import XCTest

@testable import NotesApp

final class BaseNotesTests : XCTestCase{
    
    var sut = BaseNote()
    
    func testNoteIdPopulatedSuccessfully(){
        sut.isFreshNote = true
        let mockUserDefaultsManager = MockUserDefaultsManager(userDefaultKeys: MockUserDefaultsKeys())
        let currentId = mockUserDefaultsManager.runningNoteId.intValue
        let _ = sut.save(userDefaultsManager: mockUserDefaultsManager)
        XCTAssertEqual(mockUserDefaultsManager.runningNoteId.intValue, currentId+1)
    }
}
