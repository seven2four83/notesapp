//
//  ListingTitleBarDelegate.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 28/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

///Delegate to handle title bar actions
protocol ListingTitleBarDelegate: class{
    func didTapAddButton();
    func didTapLeftButton()
    func search(quertyString: String)
}
