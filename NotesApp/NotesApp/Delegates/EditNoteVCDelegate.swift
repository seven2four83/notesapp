//
//  EditNoteVCDelegate.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation


/// Delegate to complement edit note vc actions
protocol EditNoteVCDelegate: class {
    func didSaveNote(note: Note) -> Bool
}
