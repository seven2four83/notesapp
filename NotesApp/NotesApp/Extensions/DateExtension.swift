//
//  DateExtension.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 26/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

extension Date {
    func isInSameWeek(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    func isInSameDay(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
    }
    var isInThisWeek: Bool {
        return isInSameWeek(date: Date())
    }
    var isYesterday: Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    var isToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isTomorrow: Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    var isInTheFuture: Bool {
        return Date() < self
    }
    var isInThePast: Bool {
        return self < Date()
    }
}
