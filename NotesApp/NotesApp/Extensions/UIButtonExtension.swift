//
//  UIButtonExtension.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 26/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    //Currently dead code
    func setTitleAndResize(_ title: String){
        self.setTitle(title, for: .normal)
        self.sizeToFit()
    }
}
