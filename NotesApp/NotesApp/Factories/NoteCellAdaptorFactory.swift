//
//  NoteCellAdaptorFactory.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 26/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

/// Returns an a cell adaptor based on the type enum
struct NoteCellAdaptorFactory {
    static func getCellAdapptor(forCellType cellType: Constants.NoteCellStyle) -> NoteCellAdaptor{
        switch cellType {
        case .titleOnly:
            return TitleOnlyNoteCellAdapter()
        case .textOnly:
            return TextOnlyNoteCellAdapter()
        default:
            return BaseNoteCellAdapter(reuseId: "",nib: nil)
        }
    }
}
