//
//  TitleOnlyNoteCellAdapter.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 24/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit


/// Does title-only cell specific cofiguration
final class TitleOnlyNoteCellAdapter: BaseNoteCellAdapter {
    
    init() {
        super.init(reuseId: "titleOnlyNoteCell",nib: nil)
    }
    
    override func configure(cell: UITableViewCell, withNote note: Note) {
        super.configure(cell: cell, withNote: note)
        cell.textLabel?.text = note.title
    }
    
}
