//
//  BaseNoteCellAdapter.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 24/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

/// Concrete class to manage different custom cells, given a note
class BaseNoteCellAdapter: NoteCellAdaptor {
    
    var reuseIdentifier: String
    var nib: UINib?
    
    init(reuseId: String, nib: UINib?) {
        reuseIdentifier = reuseId
        self.nib = nib
    }
    
    func configure(cell: UITableViewCell, withNote note: Note) {
        clearCellForReuse(cell: cell)
    }
    
    func clearCellForReuse(cell: UITableViewCell) {
        cell.textLabel?.text = ""
    }
    
}
