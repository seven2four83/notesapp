//
//  TextOnlyNoteCellAdapter.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 24/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

///Does text-only cell specific cofiguration
final class TextOnlyNoteCellAdapter: BaseNoteCellAdapter{
    init() {
        super.init(reuseId:
            "textOnlyNoteCell", nib: UINib(nibName: "TextOnlyNoteCell", bundle: nil))
    }
    
    override func configure(cell: UITableViewCell, withNote note: Note) {
        super.configure(cell: cell, withNote: note)
        guard let textOnlyCell = cell as? TextOnlyNoteCell else{
            return
        }
        textOnlyCell.titleLabel.text = note.title
        textOnlyCell.contentLabel.text = note.noteContent.text
        var dateString = ""
        //FIXME : Either change the label name to dateModified or fill in the created date
        if let dateUpdated = note.dateModified{
             dateString = Utility.relativeDateString(forDate: dateUpdated)
        }
        textOnlyCell.createdDateLabel.text = dateString
    }
    
    override func clearCellForReuse(cell: UITableViewCell) {
        super.clearCellForReuse(cell: cell)
        guard let textOnlyCell = cell as? TextOnlyNoteCell else{
            return
        }
        textOnlyCell.titleLabel.text = ""
        textOnlyCell.contentLabel.text = ""
        textOnlyCell.createdDateLabel.text = ""
    }
    
}
