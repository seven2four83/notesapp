//
//  NoteCellAdaptor.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 24/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

/// Protocol to manage different custom cells, given a note
protocol NoteCellAdaptor{
    func configure(cell: UITableViewCell, withNote note: Note)
    func clearCellForReuse(cell: UITableViewCell)
    var reuseIdentifier:String {get}
    var nib: UINib? {get}
}
