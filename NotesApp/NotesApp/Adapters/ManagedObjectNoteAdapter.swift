//
//  ManagedObjectNoteAdapter.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 01/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import CoreData


/// Handles configuration transitions between a note and core data managed object
struct ManagedObjectNoteAdapter {
    
    private struct ObjectKeyStrings{
        static let dateCreatedKey = "dateCreated"
        static let dateModifiedKey = "dateModified"
        static let contentKey = "content"
        static let titleKey = "title"
        static let noteIdKey = "noteId"
    }
    
    static func configure(managedObject:NSManagedObject, withNote note: Note){
        managedObject.setValue(note.dateCreated, forKey: "dateCreated")
        managedObject.setValue(note.dateModified, forKey: ObjectKeyStrings.dateModifiedKey)
        managedObject.setValue(note.title, forKey: ObjectKeyStrings.titleKey)
        managedObject.setValue(note.noteId, forKey: ObjectKeyStrings.noteIdKey)
        managedObject.setValue(note.noteId, forKey: ObjectKeyStrings.noteIdKey)
        managedObject.setValue(note.noteContent.text, forKey: ObjectKeyStrings.contentKey)
    }

    
    static func createNote(withMangaedObject managedObject: NSManagedObject) -> Note{
        var note = BaseNote()
        note.title = managedObject.value(forKey: ObjectKeyStrings.titleKey) as? String ?? "No title";
        let defaultDate = Date();
        note.dateCreated = managedObject.value(forKey: ObjectKeyStrings.dateCreatedKey) as? Date ?? defaultDate
        note.dateModified = managedObject.value(forKey: ObjectKeyStrings.dateModifiedKey) as? Date ?? defaultDate
        note.noteId = managedObject.value(forKey: ObjectKeyStrings.noteIdKey) as? NSNumber ?? -1;
        note.noteContent.text = managedObject.value(forKey: ObjectKeyStrings.contentKey) as? String ?? ""
        return note;
        
    }
    
    static func getDeleteionPredicate(forNote note: Note) -> NSPredicate{
        return NSPredicate(format: "\(ObjectKeyStrings.noteIdKey)=\(note.noteId.intValue)")
    }
}
