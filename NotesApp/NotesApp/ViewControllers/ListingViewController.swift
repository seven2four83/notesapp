//
//  ListingViewController.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 27/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit
import CoreData


/// Main View Controller used as the home screen. Lists all notes. Supports searching.
final class ListingViewController: UIViewController,NotesTableModel {
    
    @IBOutlet var titleBar: ListingTitleBar!
    @IBOutlet var allNotesTable: ListingNotesTable!
    @IBOutlet var searchNotesTable: SearchNotesTable!
    var managedObjectContext: NSManagedObjectContext? = nil
    let persistenceManager: PersistenceManager = CoreDataManager(managedObjectContext: Utility.getManagedObjectContext())
    var notes : [Note]
    var filteredNotes = [Note]()
    var currentSearchQuery = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        titleBar.delegate = self;
        allNotesTable.notesTableHandler = NotesTableHandler(withModel: self, persistenceManager: persistenceManager, parentViewController: self, isSearchMode: false,editNoteVCDelegate: self)
        configureLeftButton(currentCellStyle: UserDefaultsManager(userDefaultKeys: UserDefaultsKeys()).cellStyle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        //TODO: Read from DB async and show a loading indicator
        notes = persistenceManager.getAllNotes(sortOrder: .lastEdited, sortDirection: .descending)
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    private func configureLeftButton(currentCellStyle: Constants.NoteCellStyle){
        if currentCellStyle == .textOnly{
            titleBar.leftButton.setImage(UIImage(named: "list"), for: .normal)
        }else{
            titleBar.leftButton.setImage(UIImage(named: "detail"), for: .normal)
        }
    }
    
}

//MARK: - EditNoteVCDelegate
extension ListingViewController:EditNoteVCDelegate{
    func didSaveNote(note: Note) -> Bool {
        removeExistingNote(note:note)
        notes.insert(note, at: 0)
        reloadAllTables()
        return true
    }
    
    private func removeExistingNote(note:Note){
        if let existingNoteIndex = notes.firstIndex(where: { $0.noteId == note.noteId
        }){
            notes.remove(at: existingNoteIndex)
        }
        if let existingSearchNoteIndex = filteredNotes.firstIndex(where: { $0.noteId == note.noteId
        }){
            filteredNotes.remove(at: existingSearchNoteIndex)
        }
    }
    
    private func reloadAllTables(){
        allNotesTable.reloadData()
        if !searchNotesTable.isHidden{
            search(quertyString: currentSearchQuery)
        }
    }
}

//MARK: - ListingTitleBarDelegate methods
extension ListingViewController:ListingTitleBarDelegate{
    //TODO SMELLS: This code may actually beling to the titlebar class?
    func didTapAddButton() {
        let newNoteVC = EditNoteViewController(nibName: "EditNoteViewController", bundle: nil)
        newNoteVC.delegate = self
        self.navigationController?.pushViewController(newNoteVC, animated: false);
    }
    
    func didTapLeftButton() {
        let userDefaultsMangaer = UserDefaultsManager(userDefaultKeys: UserDefaultsKeys())
        var cellStyle = userDefaultsMangaer.cellStyle
        if cellStyle == .textOnly{
            cellStyle = .titleOnly
        }else{
            cellStyle = .textOnly
        }
        configureLeftButton(currentCellStyle: cellStyle)
        userDefaultsMangaer.cellStyle = cellStyle
        allNotesTable.changeCellStyle(style: cellStyle)
        searchNotesTable.changeCellStyle(style: cellStyle)
    }
    
    func search(quertyString: String) {
        currentSearchQuery = quertyString
        if quertyString.count > 0{
            searchNotesTable.isHidden = false
            filteredNotes = SearchHandler.filterNotes(allNotes: notes, withSearchQuery: quertyString)
            searchNotesTable.notesTableHandler = NotesTableHandler(withModel: self, persistenceManager: persistenceManager, parentViewController: self,isSearchMode: true,editNoteVCDelegate: self)
            searchNotesTable.reloadData()
        }else{
            searchNotesTable.notesTableHandler = nil
            searchNotesTable.isHidden = true
        }
    }
}
