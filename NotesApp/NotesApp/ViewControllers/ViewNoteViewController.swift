//
//  ViewNoteViewController.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

/// VC to view the note in read-only mode
final class ViewNoteViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var contenttextField: UITextView!
    var note:Note? = nil
    weak var editNoteViewControllerDelegate: EditNoteVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        congfigureSubViews()
        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonPressed))
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    private func congfigureSubViews(){
        guard let note = note else {
            UIUtility.showSingleActionAlert(title: "Error", message: "Error loading data", viewController: self)
            return
        }
        titleLabel.text = note.title
        contenttextField.text = note.noteContent.text
    }
    
    @objc private func editButtonPressed(){
        guard let note = note else {
            UIUtility.showSingleActionAlert(title: "Error", message: "Error opening in edit mode", viewController: self)
            return
        }
        let editNoteVC = EditNoteViewController(nibName: "EditNoteViewController", bundle: nil)
        editNoteVC.delegate = self
        editNoteVC.note = note
        self.navigationController?.pushViewController(editNoteVC, animated: false);
    }
}

extension ViewNoteViewController: EditNoteVCDelegate{
    func didSaveNote(note: Note) -> Bool {
        self.note = note
        congfigureSubViews()
        return editNoteViewControllerDelegate?.didSaveNote(note: note) ?? false
    }
}
