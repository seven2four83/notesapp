//
//  EditNoteViewController.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

/// VC to edit a given note
final class EditNoteViewController: UIViewController {

    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var contentTextView: UITextView!
    
    var note:Note = BaseNote()
    weak var delegate: EditNoteVCDelegate? = nil
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        note.isFreshNote = true
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleTextField.text = note.title
        contentTextView.text = note.noteContent.text
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonPressed))
        self.navigationItem.rightBarButtonItem = saveButton
    }
    
    @objc private func saveButtonPressed() -> Bool{
        populateNoteData()
        DispatchQueue.global(qos: .default).async {
            let success = self.note.save(userDefaultsManager: UserDefaultsManager(userDefaultKeys: UserDefaultsKeys()))
            DispatchQueue.main.async {
                if success{
                    let _ = self.delegate?.didSaveNote(note: self.note)
                    self.navigationController?.popViewController(animated: false)
                }else{
                    UIUtility.showSingleActionAlert(title: "Error saving", message: "Could not save", viewController: self)
                }
            }
        }
        return false
    }
    
    private func populateNoteData(){
        note.title = titleTextField.text ?? "No Title"
        note.noteContent.text = contentTextView.text
    }

}
