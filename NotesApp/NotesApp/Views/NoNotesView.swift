//
//  NoNotesView.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 28/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

class NoNotesView: UIView {
    
    @IBOutlet var messageLabel: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    private func customInit(){
        UIUtility.customViewInit(fromNib: "NoNotesView", owner: self)
    }
}
