//
//  BaseNotesTable.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 07/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit


/// Represents a table that lists notes. Common for allNotes and searchTable. Also the point of contact for changing cell style
class BaseNotesTable : UITableView{
    var notesTableHandler:NotesTableHandler? = nil{
        didSet{
            setupTable()
        }
    }
    
    private func setupTable(){
        guard let notesTableHandler = notesTableHandler else{
            return
        }
        delegate = notesTableHandler
        dataSource = notesTableHandler
        rowHeight = UITableView.automaticDimension
        //TODO: Avoid hard-coding
        changeCellStyle(style: UserDefaultsManager(userDefaultKeys: UserDefaultsKeys()).cellStyle, shouldReloadData: false)
        tableFooterView = UIView()
    }
    
    func changeCellStyle(style: Constants.NoteCellStyle, shouldReloadData: Bool =  true)  {
        guard let notesTableHandler = notesTableHandler else{
            return
        }
        let adaptor = NoteCellAdaptorFactory.getCellAdapptor(forCellType: style)
        notesTableHandler.cellAdaptor = adaptor
        register(adaptor.nib, forCellReuseIdentifier: adaptor.reuseIdentifier)
        if shouldReloadData{
            reloadData()
        }
    }
}
