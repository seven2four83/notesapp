//
//  ListingTitleBar.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 24/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit


/// Title Bar to be used instead of Navigation Bar in ListingVC
final class ListingTitleBar: UIView {
    
    weak var delegate:ListingTitleBarDelegate? = nil
    @IBOutlet var leftButton: UIButton!
    
    @IBOutlet var addButton: UIButton!
    @IBOutlet var searchButton: UIButton!
    @IBAction func didTapAddButton(_ sender: Any) {
        delegate?.didTapAddButton()
    }
    @IBOutlet private var searchTextField: UITextField!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        leftButton.imageView?.contentMode = .scaleAspectFit
        addButton.imageView?.contentMode = .scaleAspectFit
        searchButton.imageView?.contentMode = .scaleAspectFit
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit();
    }
    @IBAction func didTapSeach(_ sender: Any) {
        searchTextField.isHidden = false
        searchTextField.becomeFirstResponder()
        searchTextField.delegate = self
    }
    @IBAction func didEditSearchTextField(_ sender: Any) {
        delegate?.search(quertyString: searchTextField.text ?? "")
    }
    @IBAction func didTapLeftButton(_ sender: Any) {
        delegate?.didTapLeftButton()
    }
    
    private func customInit(){
        UIUtility.customViewInit(fromNib: "ListingTitleBar", owner: self)
    }
}

extension ListingTitleBar: UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.isHidden = true
        return true
    }
}
