//
//  TextOnlyNoteCell.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 26/07/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import UIKit

/// Custom cell displaying the title, content, and date
final class TextOnlyNoteCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var createdDateLabel: UILabel!
    
}
