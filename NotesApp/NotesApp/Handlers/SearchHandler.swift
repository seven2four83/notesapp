//
//  SearchHandler.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 13/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

/// Used to filter notes. In future, if cloud support enabled, will also be used to search for notes on the cloud
struct SearchHandler {
    static func filterNotes(allNotes : [Note], withSearchQuery searchQuery: String) -> [Note]{
        var results = [Note]()
        //TODO: Use array.filter instead
        for note in allNotes{
            if let _ = note.title.range(of: searchQuery, options: .caseInsensitive){
                results.append(note)
            }
            //TODO: Content search
        }
        return results
    }
}
