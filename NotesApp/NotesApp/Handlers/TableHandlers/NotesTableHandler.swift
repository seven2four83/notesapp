//
//  NotesTableHandler.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 07/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit


/// A model to pass around the model arrays as reference. Alternate approach would be creating observables(say, using RxSwift)
protocol NotesTableModel: class {
    var notes:[Note]{get set}
    var filteredNotes:[Note]{get set}
}


/// Handles table data source and delegate methods for the  both notes tables - allNotes and search results
final class NotesTableHandler: BaseTableHandler {
    var notes: [Note]{
        get{
            if isSearchMode{
                return model.filteredNotes
            }
            return model.notes
        }
        set(newValue){
            if isSearchMode{
                model.filteredNotes = newValue
            }else{
                model.notes = newValue
            }
        }
    }
    
    var model: NotesTableModel
    let persistenceManager: PersistenceManager
    let parentVC: UIViewController
    var isSearchMode:Bool
    var editNoteViewControllerDelegate: EditNoteVCDelegate
    var cellAdaptor: NoteCellAdaptor? = nil
    
    init(withModel model: NotesTableModel, persistenceManager: PersistenceManager, parentViewController: UIViewController, isSearchMode: Bool, editNoteVCDelegate: EditNoteVCDelegate) {
        self.model = model
        self.persistenceManager = persistenceManager
        parentVC = parentViewController
        self.isSearchMode = isSearchMode
        self.editNoteViewControllerDelegate = editNoteVCDelegate
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if notes.count == 0{
            let noNotesView = NoNotesView(frame: tableView.bounds)
            //TODO: Show separate message for search results. Do not do an is? check, use two different handlers and get the respective value from the handler
            noNotesView.messageLabel.text = "No Notes"
            noNotesView.messageLabel.sizeToFit()
            tableView.backgroundView = noNotesView
        }else{
            tableView.backgroundView = nil
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = notes[indexPath.row]
        guard let cellAdaptor = cellAdaptor, let cell = tableView.dequeueReusableCell(withIdentifier: cellAdaptor.reuseIdentifier) else {
            return getDefaultCell(forNote: note)
        }
        cellAdaptor.configure(cell: cell, withNote: note)
        return cell
    }
    
    private func getDefaultCell(forNote note:Note) -> UITableViewCell{
        let cell = UITableViewCell(style: .default, reuseIdentifier: TitleOnlyNoteCellAdapter().reuseIdentifier)
        cell.textLabel?.text = note.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions = [UITableViewRowAction]()
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.deleteActionHandler(table: tableView, indexPath: indexPath)
        }
        actions.append(deleteAction)
        return [deleteAction]
    }
    
    private func deleteActionHandler(table: UITableView, indexPath : IndexPath){
        DispatchQueue.global(qos: .default).async {
            let note = self.notes[indexPath.row]
            let didDelete = note.delete()
            DispatchQueue.main.async {
                if didDelete{
                    self.notes.remove(at: indexPath.row)
                    table.deleteRows(at: [indexPath], with: .fade)
                }else{
                    UIUtility.showSingleActionAlert(title: "Error", message: "Could not delete", viewController: self.parentVC)
                }
            }
        }
    }
}

//MARK: - TableViewDelegate

extension NotesTableHandler{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewNoteVC = ViewNoteViewController(nibName: "ViewNoteViewController", bundle: nil)
        viewNoteVC.note = notes[indexPath.row]
        viewNoteVC.editNoteViewControllerDelegate = editNoteViewControllerDelegate
        tableView.deselectRow(at: indexPath, animated: false)
        parentVC.navigationController?.pushViewController(viewNoteVC, animated: true)
    }
}
