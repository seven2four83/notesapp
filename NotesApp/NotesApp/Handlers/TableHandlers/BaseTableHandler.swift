//
//  BaseTableHandler.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 06/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

/// Class to be used as a delegate handling table data source and delegate methods
class BaseTableHandler: NSObject,UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
}
