//
//  BaseNote.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation


/// Cocncrete class representing a note
struct BaseNote:Note {
    var title: String = ""
    var dateCreated: Date = Date()
    var dateModified: Date? = nil
    var persistenceManager:PersistenceManager? = CoreDataManager(managedObjectContext: Utility.getManagedObjectContext())
    var noteId: NSNumber = -1
    var noteContent = NoteContent()
    var isFreshNote = false
    
    mutating func save(userDefaultsManager: UserDefaultsManager) -> Bool {
        guard let persistenceManager = persistenceManager else{
            return false
        }
//        Feature decision: When saving fails, an alert will be thrown, listing table will not be reloaded, and yet the updateTime of the note will not be reset to previous successfully updated time
        let saveInitiatedTime = Date()
        if isFreshNote{
            if(noteId.intValue < 0){
                populateNoteId(userDefaultsManager: userDefaultsManager)
            }
            if persistenceManager.saveNote(note: self){
                dateModified = saveInitiatedTime
                isFreshNote = false
                return true
            }
            return false
        }else{
            return persistenceManager.updateNote(note: self)
        }
    }
    
    //Dependency injection for testing using mocks
    private mutating func populateNoteId(userDefaultsManager: UserDefaultsManager){
        var currentCount = userDefaultsManager.runningNoteId
        currentCount = NSNumber(value: currentCount.intValue + 1)
        userDefaultsManager.runningNoteId = currentCount
        noteId = currentCount
    }
    
    func delete() -> Bool {
        return persistenceManager?.deleteNote(note: self) ?? false
    }
    
    
}
