//
//  Note.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation


/// Protocol representing a Note
protocol Note {
    var title: String {get set}
    var dateCreated: Date {get set}
    var dateModified: Date? {get set}
    var noteId: NSNumber {get set}
    var noteContent: NoteContent {get set}
    var persistenceManager:PersistenceManager? {get set}
    var isFreshNote: Bool {get set}
    
    mutating func save(userDefaultsManager: UserDefaultsManager) -> Bool
    func delete() -> Bool
}
