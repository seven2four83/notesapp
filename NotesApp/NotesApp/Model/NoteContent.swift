//
//  NoteContent.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 27/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

/// Struct representing the body of the note
struct NoteContent{
    var text = ""
}
