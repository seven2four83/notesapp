//
//  UIUtility.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit

/// Utility functions for UI
struct UIUtility {
    static func showSingleActionAlert(title: String, message:String, viewController:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in
        }
        alert.addAction(okayAction)
        viewController.present(alert, animated: false, completion: nil)
    }
    
    static func customViewInit(fromNib nibName: String, owner: UIView){
        if let contentView = Bundle.main.loadNibNamed(nibName, owner: owner, options: nil)?.first as? UIView{
            contentView.frame = owner.bounds
            owner.addSubview(contentView);
        }
    }
}
