//
//  Constants.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 07/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

/// Constants to be used across the project. Many constants will be available as private vars in the appropriate files
struct Constants {
    enum NoteCellStyle:Int {
        case textOnly=0, titleOnly, thumbnail, none
    }
    
    enum SortOrder:Int{
        case lastEdited = 0, lastCreated, title, none
    }
    
    enum SortDirection:Int{
        case ascending = 0, descending, none
    }
    
    struct AccessibilityIdentifiers{
        struct ListingVC{
            static let allNotesTable = "allNotesTable"
            static let searchTable = "searchTable"
            struct ListingTitleBar {
                static let leftButton = "leftButton"
                static let searchButton = "searchButton"
                static let searchTextField = "searchTextField"
            }
            struct TextOnlyCell{
                static let titleLabel = "textOnlyCellTitleLabel"
                static let contentLabel = "textOnlyCellContentLabel"
                static let dateLabel = "textOnlyCellDateLabel"
            }
        }
        struct ViewNoteVC {
            static let titleLabel = "titleLabel"
            static let viewContentTextView = "ContentTextFieldIdentifier"
        }
        
        struct EditNoteVC {
            static let titleTextField = "titleTextField"
            static let contentTextView = "EditContentTextFieldIdentifier"
            static let saveButton = "saveButton"
        }
    }
}
