//
//  Utility.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 08/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import UIKit
import CoreData


/// Non-UI Utility functions
struct Utility{
    static func getManagedObjectContext() -> NSManagedObjectContext?{
        return (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext;
    }
    
    static func relativeDateString(forDate date: Date) -> String{
        let currentDate = Date()
        if date.isToday{
            return "Today"
        }else if date.isYesterday{
            return "Yesterday"
        }else if date.isInThisWeek{
            return "This week"
        }else if date.isInSameMonth(date: currentDate){
            return "This month"
        }else if date.isInSameYear(date: currentDate){
            return "This year"
        }else{
            if let yearDifference = Calendar.current.dateComponents([.year], from: date, to: currentDate).year{
                if yearDifference < 2{
                    return "Last year"
                }else{
                    return "\(yearDifference) years ago"
                }
            }
        }
        return ""
    }
}
