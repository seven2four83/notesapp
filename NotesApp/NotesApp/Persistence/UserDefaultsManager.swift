//
//  UserDefaultsManager.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 15/06/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation


/// Set of keys used for UserDefaults. These will be mocked for XCTests
class UserDefaultsKeys{
    var runningNoteIdUserDefaultsKey = "runningNoteIdUserDefaultsKey"
    var cellStyleUserDefaultsKey = "cellStyleUserDefaultsKey"
}

/// Manages values stored as UserDefaults
class UserDefaultsManager{
    
    let userDefaultKeys: UserDefaultsKeys
    
    init(userDefaultKeys: UserDefaultsKeys) {
        self.userDefaultKeys = userDefaultKeys
    }
    
    var runningNoteId:NSNumber{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue, forKey: userDefaultKeys.runningNoteIdUserDefaultsKey)
            userDefaults.synchronize()
            
        }
        get{
            return NSNumber(integerLiteral: UserDefaults.standard.integer(forKey:  userDefaultKeys.runningNoteIdUserDefaultsKey))
        }
    }
    
    var cellStyle: Constants.NoteCellStyle{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue.rawValue, forKey: userDefaultKeys.cellStyleUserDefaultsKey)
            userDefaults.synchronize()
        }
        get{
            return  Constants.NoteCellStyle(rawValue: UserDefaults.standard.integer(forKey: userDefaultKeys.cellStyleUserDefaultsKey)) ?? Constants.NoteCellStyle.textOnly
        }
    }
    
}
