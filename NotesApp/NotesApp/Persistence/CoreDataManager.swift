//
//  CoreDataManager.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation
import CoreData

///Handles Core data operations
struct CoreDataManager {
    
    private struct Keys{
        struct NotesTable {
            static let dateModifiedKey = "dateModified"
            static let noteIdKey = "noteId"
        }
    }
    //TODO: Place the constant in the above struct
    private let NOTES_TABLE_NAME = "Notes"
    
    private let managedObjectContext:NSManagedObjectContext?
    
    init(managedObjectContext:NSManagedObjectContext?) {
        self.managedObjectContext = managedObjectContext
    }
    
}

extension CoreDataManager:PersistenceManager{
    
    func saveNote(note: Note) -> Bool {
        guard let managedObjectContext = managedObjectContext else{
            return false
        }
        let noteManagedObject = NSEntityDescription.insertNewObject(forEntityName: NOTES_TABLE_NAME, into: managedObjectContext)
        //FIXME: avoid hardcoding of isFreshNore
        ManagedObjectNoteAdapter.configure(managedObject: noteManagedObject, withNote: note)
        do {
            try managedObjectContext.save()
            
        } catch _{
            return false
        }
        return true
    }
    
    func deleteNote(note: Note) -> Bool {
        guard let managedObjectContext = managedObjectContext else{
            return false
        }
        let request =  NSFetchRequest<NSFetchRequestResult>(entityName: NOTES_TABLE_NAME)
        request.predicate = ManagedObjectNoteAdapter.getDeleteionPredicate(forNote: note)
        do{
            if let results = try managedObjectContext.fetch(request) as? [NSManagedObject]{
                if(results.count > 0){
                    managedObjectContext.delete(results[0])
                    do{
                        try managedObjectContext.save()
                    }catch{
                        print(error)
                    }
                }
            }
            
        }catch{
            print(error)
        }
        return true
    }
    
    func getAllNotes(sortOrder: Constants.SortOrder, sortDirection: Constants.SortDirection) -> [Note] {
        var notes = [Note]()
        guard let managedObjectContext = managedObjectContext else{
            return notes
        }
        let query = NSFetchRequest<NSFetchRequestResult>(entityName: NOTES_TABLE_NAME)
        query.sortDescriptors = [NSSortDescriptor(key: getKeyPath(forSortOrder: sortOrder), ascending: getIsSortAscending(forSortDirection: sortDirection))]
        do{
            let results = try managedObjectContext.fetch(query)
            for result in results as! [NSManagedObject]{ notes.append(ManagedObjectNoteAdapter.createNote(withMangaedObject: result))
            }
        }catch{
            print("Error retireving data")
        }
        return notes
    }
    
    private func getKeyPath(forSortOrder sortOrder: Constants.SortOrder) -> String{
        switch sortOrder {
        case .lastEdited:
            return Keys.NotesTable.dateModifiedKey
        default:
            return Keys.NotesTable.dateModifiedKey
        }
    }
    
    private func getIsSortAscending(forSortDirection sortDirection: Constants.SortDirection) -> Bool{
        switch sortDirection {
        case .ascending:
            return true
        default:
            return false
        }
    }
    
    func updateNote(note: Note) -> Bool {
        guard let managedObjectContext = managedObjectContext else{
            return false
        }
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: NOTES_TABLE_NAME)
        fetchRequest.predicate = NSPredicate(format: "\(Keys.NotesTable.noteIdKey) = %@", note.noteId)
        
        do{
            let results = try managedObjectContext.fetch(fetchRequest)
            let noteManagedObject = results[0] as! NSManagedObject
            ManagedObjectNoteAdapter.configure(managedObject: noteManagedObject, withNote: note)
            do{
                try managedObjectContext.save()
            }
            catch{
                print(error)
            }
            return true
        }catch{
            print(error)
        }
        
        return false
    }
}
