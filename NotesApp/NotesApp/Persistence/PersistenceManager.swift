//
//  PersistenceManager.swift
//  NotesApp
//
//  Created by Jagan Moorthy on 29/05/19.
//  Copyright © 2019 Jagan Moorthy. All rights reserved.
//

import Foundation

///Protocol for notes storage management
protocol PersistenceManager {
    func getAllNotes(sortOrder: Constants.SortOrder, sortDirection: Constants.SortDirection) -> [Note]
    func saveNote(note:Note) -> Bool
    func updateNote(note:Note) -> Bool
    func deleteNote(note:Note) -> Bool
}
