
**NotesApp**

**Purpose**: Having coded too many prototypes that only focus on certain iOS APIs(UNNotifs, URLSessions), I wanted an app that integrates most of the learnings.

UX issues:
- The app is more from a technical learning standpoint, so not much time hsa been invested in making it look pretty.
- Testing was mostly done using iPhone Xr simulator. Constraints were checked across devices. User experience not as intended in iPads as the split view controller is not properly used(The project started as a SplitVC, but then I started pushing stuff into the MasterVC's Navigation stack instead of DetailVC's)
- For best results, use iPhone Xr simulator.

A cleaner UX is available in my other project [ShowFinder](https://gitlab.com/seven2four83/show-finder).

**Key Design/Implementation Decisions:**

 1. Basic philosophy: No class knows too much info/implementation detail. A good system is when a lot of dumb classes do something smart together.
 2. Avoid other third party frameworks. Try to do get stuff done by yourself. 
 3. Avoid MVVM/VIPER. Stick to MVC, yet avoid Massive VCs. Less than 100 lines of code/file(This limit applies to non-VC classes too). Primarily VCs only do view setup and interactions. Use handlers for anything else
 4. Avoid using ready-made NSFetchedResultsController. Instead manage all Core Data operations and appropriate UX changes manually.
 5. Protocol Oriented Programming
 6. Prefer structs to classes (Exception: Classes handy in XCTest mocking)
 7. Commenting: If your code needs comments, either the function/variable name is wrong or the function does too much stuff. Rename the function/variable. Code updates, comments dont. Check out Uncle Bob's guidelines(Clean Code) for code comments.
 8. Avoid Observables for unicast scenarios. Use delegates/completion handers.  Don't take the RxSwift-RxCocoa shortcut. Do things manually (No third party).
 9. Prefer dependency injection to singletons
 10. Functions: Line limit: 15 - 20 lines (exceptions for try-catch and switch blocks)
 11. Project folder structure: The current structure of design-pattern based folder names is only for showcasing purposes. A production app would have folders based on functionality.

**Practical challenge**

I don't have a physical iOS device, and my Mac has pretty less RAM, so things like opening of app was sometimes slow. But time profiling didn't catch/raise any major concerns.

**TODO:**

 1. SplitVC is not actually used. The VCs have been pushed onto the Master VC's NavController instead
 2. Most test cases only cover positive cases. Add negative cases.
 4. Most coverage via XCUITest. Incorporate more XCTests(saves time in the long run)
 5. Code coverage is only around 50%, get up to 90%
 6. Localization
 7. Reminder
 8. Pagination of notes(load only a certain number of notes at a time, more useful for cloud storage model - long term target)
 9. Better UX (images, animations)
 10. Use in-memory CoreData for XCTest
 11. Console errors on app launch
